<?php
App::uses('AppModel', 'Model');
/**
 * Mahasiswa Model
 *
 */
class Mahasiswa extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

}
