<?php

class MahasiswasController extends AppController {

    public $components = array('RequestHandler');

    public function index() {
        $mahasiswas = $this->Mahasiswa->find('all');
        $this->set(array(
            'mahasiswas' => $mahasiswas,
            '_serialize' => array('mahasiswas')
        ));
    }

    public function view($id) {
        $mahasiswa = $this->Mahasiswa->findById($id);
        $this->set(array(
            'mahasiswa' => $mahasiswa,
            '_serialize' => array('mahasiswa')
        ));
    }

    public function add() {
        $this->layout = false;
        $data = $this->request->input('json_decode', true);
        if(empty($data)){
            $data = $this->request->data;
        }
        if ($this->Mahasiswa->save($data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            'data' => $data,
            '_serialize' => array('message','data')
        ));
    }

    public function edit($id) {
        $this->layout = false;
        $data = $this->request->input('json_decode', true);
        if(empty($data)){
            $data = $this->request->data;
        }
        $this->Mahasiswa->id = $id;
        if ($this->Mahasiswa->save($data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            'data' => $data,
            '_serialize' => array('message','data')
        ));
    }

    public function delete($id) {
        if ($this->Mahasiswa->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
}
?>